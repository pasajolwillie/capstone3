import { useState, useEffect, useContext } from 'react';
import {Container, Form, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, Navigate, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Update() {

 const { user } = useContext(UserContext);

 const navigate = useNavigate();

const [isActive, setIsActive] = useState(false)

 const { bookId } = useParams()

 const [title, setTitle] = useState("");
 const [author, setAuthor] = useState("");
 const [price, setPrice] = useState(0);
 const [quantity, setQuantity] = useState(0);




 const update = (e) => {
  e.preventDefault();


   fetch(`https://lit-garden-66878.herokuapp.com/api/books/${bookId}/updates`, {
     method: "PUT",
     headers: {
       "Content-Type": "application/json",
       Authorization: `Bearer ${localStorage.getItem("token")}`
     },
     body: JSON.stringify({
       bookId: bookId,
       title: title,
       author: author,
       price: price,
       quantity: quantity
     })

   })
   .then(res => res.json())
   .then(data => {
     

     if (data === true) {
       Swal.fire({
         title: "Successfully Updated",
         icon: "success",
         text: "You have successfully update this item."
       })

       navigate("/books")

     } else {
       Swal.fire({
         title: "Something went wrong",
         icon: "error",
         text: "Please try again"
       })
     }
   })
 }

 useEffect(()=> {
   fetch(`https://lit-garden-66878.herokuapp.com/api/books/${bookId}`)
   .then(res => res.json())
   .then(data => {
     

     setTitle(data.title)
     setAuthor(data.author)
     setPrice(data.price)
     setQuantity(data.quantity)
   })
 }, [bookId])



	return(

		<Form onSubmit= {(e) => update(e)}>
		<h1 className="text-center">Update</h1>

		<Form.Group 
		  	className="mb-3"
		  	controlId="title">
			    <Form.Label>Title:</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder=" Enter Book Title"
			    	value={title}
			    	onChange={e => {
			    		setTitle(e.target.value)
			    		//console.log(e.target.value)
			    	}}
			    	required />
			   
			  </Form.Group>


				  <Form.Group 
			  	className="mb-3"
			  	controlId="author">
				    <Form.Label>Author:</Form.Label>
				    <Form.Control 
				    	type="text" 
				    	placeholder="Enter Author Name"
				    	value={author}
				    	onChange={e => {
				    		setAuthor(e.target.value)
				    		//console.log(e.target.value)
				    	}}
				    	required />
				   
				  </Form.Group>


		  <Form.Group 
		  	className="mb-3"
		  	controlId="price">
			    <Form.Label>Price:</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter Price"
			    	value={price}
			    	onChange={e => {
			    		setPrice(e.target.value)
			    		//console.log(e.target.value)
			    	}}
			    	required />

			  </Form.Group>



			  <Form.Group className="mb-3" controlId="quantity">
			    <Form.Label>Quantity</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder="Enter Quantity" 
			    	value={quantity}
			    	onChange={e => {
			    		setQuantity(e.target.value)
			    	}}
			    	required
			    	/>


		 </Form.Group>
			  
			  	<Button variant="primary" type="submit" 
			  	id="submitBtn"> Submit </Button>
		
		</Form>
		)
}

