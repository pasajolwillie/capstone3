import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import UserContext from  '../UserContext';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';



export default function Login() {


	const { user, setUser } = useContext(UserContext);


	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	
	const [isActive, toActivate] = useState(true)

	function authenticate(e) {

		e.preventDefault();



		fetch('https://lit-garden-66878.herokuapp.com/api/users/user', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})

		.then(res => res.json())
		.then(data => {

		

			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
				

			Swal.fire({
				title:"Login Successful",
				icon:"success",
				text: "Welcome to BOOKWORM!"
			});

		} else {
			Swal.fire({
				title: "Authentication Failed",
				icon: "error",
				text: "Check your login details  and try again"
			})
		}


			})

		setEmail("");
		setPassword("");


	}

	const retrieveUserDetails = (token) => {

		fetch('https://lit-garden-66878.herokuapp.com/api/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {


			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})

	}

	useEffect(() =>{
	
		if(email !== "" && password !== "") {
			toActivate(true)
		} else {
			toActivate(false)
		}
 
		}, [email, password]

		)

	return(
		
		(user.id !== null) ?

		<Navigate to="/books" />

		:

		<Form  onSubmit= {(e) => authenticate(e)}>
		<h1 className="text-center">Login</h1>
		  <Form.Group 
		  	className="mb-3"
		  	controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email"
			    	value={email}
			    	onChange={e => {
			    		setEmail(e.target.value)
			    	}}
			    	required />

			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password}
			    	onChange={e => {
			    		setPassword(e.target.value)
			    	}}
			    	required
			    	/>

			   
		 </Form.Group>
			  {
			  	isActive ?
			  	<Button variant="primary" type="submit" 
			  	id="submitBtn"> Submit </Button>
			  	:
			  	<Button variant="danger" type="submit" 
			  	id="submitBtn" disabled> Submit </Button>	

			  }
		</Form>
		)
}
