import { Navbar, Nav, Container } from 'react-bootstrap';
import { Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		  <Container>
		    <Navbar.Brand as={Link} to="/" className="text-primary"><strong>BOOKWORM</strong></Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="ml-auto">
		        <Nav.Link as={Link} to="/" className="text-primary">Home</Nav.Link>
		        <Nav.Link as={Link} to="/books" className="text-primary">Books</Nav.Link>
		      	
		      	{ (user.isAdmin === true ) ?

		      		<Fragment>

		      			<Nav.Link as={Link} to="/admin" className="text-primary">Admin</Nav.Link>

		      			<Nav.Link as={Link} to="/logout" className="text-primary"> Logout</Nav.Link>
		      		</Fragment>

		      			:

				      	   (user.id !== null) ?

				      		<Fragment>
					      		<Nav.Link as={Link} to="/logout" className="text-primary"> Logout</Nav.Link>
					      	

					      	</Fragment>
				      		
				      		:
				      		<Fragment>
				      			<Nav.Link as={Link} to="/register" className="text-primary">Register</Nav.Link>
				      			<Nav.Link as={Link} to="/login" className="text-primary">Login</Nav.Link>
				      		</Fragment>

		      	}
		      	
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>


		)
}