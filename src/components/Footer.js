
export default function Footer() {
	return(
		
				<footer className="footer text-center text-primary" >
				        <span> This page is intended for educational purposes only.    </span>
				        <span>    |    </span>
				        <span> &copy; All Rights Reserved 2022 </span>
				        <span>    |    </span>
				        <span> by Willie Pasajol </span>
				 </footer>

		); 
};
